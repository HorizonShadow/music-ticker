import it.sauronsoftware.feed4j.FeedParser;
import it.sauronsoftware.feed4j.bean.Feed;
import it.sauronsoftware.feed4j.bean.FeedHeader;
import it.sauronsoftware.feed4j.bean.FeedItem;

import java.net.URL;

/**
 * Created with IntelliJ IDEA.
 * User: Josh
 * Date: 10/7/12
 * Time: 11:05 AM
 * To change this template use File | Settings | File Templates.
 */
public class Song
{
    public void Song()
    {
    }
    private Feed feed;
    public String getSong(String user) throws Exception
    {
        URL url = new URL("http://ws.audioscrobbler.com/1.0/user/" + user + "/recenttracks.rss");

        try
        {
            feed = FeedParser.parse(url);
        }
        catch(Exception e)
        {
            System.out.println(e.getCause());
            return "Error: User \"" + user + "\" not found.";
        }
        try
        {
            FeedItem item = feed.getItem(0);
            System.out.println(item.getTitle());
            return(item.getTitle());
        }
        catch(Exception e)
        {
            return("Error: No songs found on \"" + user +"\"");
        }

    }
}
