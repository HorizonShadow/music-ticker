import java.io.*;

/**
 * Created with IntelliJ IDEA.
 * User: Cereal
 * Date: 1/27/13
 * Time: 8:38 AM
 * To change this template use File | Settings | File Templates.
 */
public class Preferences
{

    private static final String fileLocation = "Preferences.txt";
    public String user = "";
    public Boolean startMinimized = false;
    public Boolean startOnStartup = false;

    public static void Preferences()
    {

    }
    public boolean exists(){
        return new File(fileLocation).exists();
    }
    public void writeDefaults() throws IOException
    {
            OutputStreamWriter os = new OutputStreamWriter(new FileOutputStream(fileLocation), "UTF-8");
            BufferedWriter out = new BufferedWriter(os);
            out.write("");
            out.newLine();
            out.write("false");
            out.newLine();
            out.write("false");
            out.close();
    }
    public static void writeToFile() throws IOException
    {
            OutputStreamWriter os = new OutputStreamWriter(new FileOutputStream(fileLocation), "UTF-8");
            BufferedWriter out = new BufferedWriter(os);
            out.write(user);
            out.newLine();
            out.write(Boolean.toString(startMinimized));
            out.newLine();
            out.write(Boolean.toString(startOnStartup));
            out.close();

    }
    public void read() throws IOException
    {
            InputStreamReader is = new InputStreamReader(new FileInputStream(fileLocation), "UTF-8");
            BufferedReader in = new BufferedReader(is);
            user = in.readLine();
            startMinimized = Boolean.parseBoolean(in.readLine());
            startOnStartup = Boolean.parseBoolean(in.readLine());
    }
    public static void setUsername(String string) throws IOException
    {
        user = string;
        writeToFile();

    }
    public static void setStartState(Boolean bool) throws IOException
    {
        startMinimized = bool;
        writeToFile();

    }
    public static void setOnStartup(Boolean bool) throws IOException
    {
        startOnStartup = bool;
        writeToFile();

    }

}
