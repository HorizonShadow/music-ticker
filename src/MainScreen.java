import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created with IntelliJ IDEA.
 * User: Cereal
 * Date: 10/14/12
 * Time: 7:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class MainScreen
{
    private JPanel panel1;
    private JTextField textField1;
    private JTextField textField2;
    private JCheckBox startMinimizedCheckBox;
    static final JFrame frame = new JFrame("Song sGrabber");


    public MainScreen() throws Exception
    {

        Preferences prefs = new Preferences();
        if(prefs.exists()){
            prefs.read();
        } else {
            prefs.writeDefaults();
        }
        if(appIsRunning())
            System.exit(0);
        if(Preferences.startMinimiszed)
            startMinimizedCheckBox.doClick();


        final Plugin plugin = new Plugin();
        Timer timer = new Timer();
        if(startupUser != null)
        {
            plugin.setUser(startupUser);
            textField1.setText(startupUser);
        }
        TimerTask timerTask = new TimerTask()
        {
            @Override
            public void run()
            {
                textField2.setText(plugin.getSong());
            }
        };
        timer.scheduleAtFixedRate(timerTask, 1000, 1000);
        textField1.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
                if(e.getKeyChar() == '\n')
                {
                    Preferences.setUsername(textField1.getText());
                    textField2.setText("Grabbing...");
                    plugin.setUser(textField1.getText());
                }

            }

            @Override
            public void keyPressed(KeyEvent e) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public void keyReleased(KeyEvent e) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });
        startMinimizedCheckBox.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                try{
                    Preferences.setStartState(!Preferences.startMinimized);
                } catch(IOException ex){
                    JOptionPane.showMessageDialog(frame,ex.getMessage());
                }

            }
        });

    }

    private Boolean appIsRunning()
    {
        int port = 12345;
        ServerSocket s;
        try
        {
            s = new ServerSocket(port, 10, InetAddress.getLocalHost());

        } catch (UnknownHostException e) {
            JOptionPane.showMessageDialog(frame, e.getMessage());
        } catch (IOException e)
        {
            JOptionPane.showMessageDialog(frame, "Another instance of this app is already running.");
            return true;
        }
        return false;
    }

    public static void main(String[] args) throws Exception {

        String startupUser = null;
        if(!new File("Preferences.txt").exists())
        {
            try{
                Preferences.writeDefaults();

            } catch(IOException e){
                JOptionPane.showMessageDialog(frame, e.getMessage());
            }
        }
        if(args.length > 0)
            startupUser = args[0];
        else
        {
            Preferences.readPrefs();
            startupUser = Preferences.user;
        }

        frame.setPreferredSize(new Dimension(500,300));
        frame.setContentPane(new MainScreen(startupUser).panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.revalidate();


        URL resource = frame.getClass().getResource("/Java.png");
        System.out.println(resource);

        Image image = Toolkit.getDefaultToolkit().getImage(resource);
        frame.setIconImage(image);
        if(SystemTray.isSupported())
        {
            final TrayIcon icon = new TrayIcon(image, "Music Ticker", createPopupMenu());
            icon.setToolTip("Music ticker");
            icon.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent e) {
                    frame.setVisible(true);
                    frame.setExtendedState(frame.NORMAL);
                    SystemTray.getSystemTray().remove(icon);
                }
            });

            if(Preferences.startMinimized)
            {
                frame.setVisible(false);
                try
                {
                    SystemTray.getSystemTray().add(icon);
                } catch (AWTException e1)
                {
                    e1.printStackTrace();
                }
            }
            frame.addWindowListener(new WindowListener()
            {
                @Override
                public void windowOpened(WindowEvent e) {}
                @Override
                public void windowClosing(WindowEvent e) {}
                @Override
                public void windowClosed(WindowEvent e) {}
                @Override
                public void windowIconified(WindowEvent e)
                {
                    frame.setVisible(false);
                    try
                    {
                        SystemTray.getSystemTray().add(icon);
                    } catch (AWTException e1)
                    {
                        e1.printStackTrace();
                    }
                }
                @Override
                public void windowDeiconified(WindowEvent e) {}
                @Override
                public void windowActivated(WindowEvent e) {}
                @Override
                public void windowDeactivated(WindowEvent e) {}
            });
        }
    }
    private static PopupMenu createPopupMenu() throws HeadlessException
    {
        PopupMenu menu = new PopupMenu();
        MenuItem exit = new MenuItem("Exit");
        MenuItem show = new MenuItem("Show");
        show.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                TrayIcon[] icon = SystemTray.getSystemTray().getTrayIcons();
                frame.setVisible(true);
                frame.setExtendedState(frame.NORMAL);
                SystemTray.getSystemTray().remove(icon[0]);

            }
        });
        exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                System.exit(0);
            }
        });
        menu.add(show);
        menu.add(exit);
        return menu;
    }
}
