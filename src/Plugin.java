import java.io.*;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created with IntelliJ IDEA.
 * User: Josh
 * Date: 10/7/12
 * Time: 11:03 AM
 * To change this template use File | Settings | File Templates.
 */
public class Plugin
{
    public Song song = new Song();
    public String currentSong = "No current song";
    public String lastSong = "";
    public String user = "";


    public Plugin() throws Exception
    {
        Timer timer = new Timer();

        TimerTask task = new TimerTask()
        {
            @Override
            public void run()
            {
                try
                {
                    currentSong = song.getSong(user);
                    if(!currentSong.equals(lastSong))
                    {
                        writeToFile(currentSong);
                        lastSong = currentSong;
                    }
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        };
        timer.scheduleAtFixedRate(task, 1000,1000);
    }
    public void setUser(String user)
    {
        this.user = user;
        try{
            song.getSong(user);


        }
        catch(Exception e){};

    }
    public String getSong()
    {
        return currentSong;
    }
    public void writeToFile(String string)
    {
        String fullArtistAndSong;
        if (string.contains("–")) {
            String[] artistAndSong = string
                    .split("–");
            char delimiter = '-';
            String artist = artistAndSong[0].trim();
            String song = artistAndSong[1].trim();
            fullArtistAndSong = artist + " " + delimiter + " " + song;
        } else
        {
            fullArtistAndSong = string;
        }
        try
        {
            OutputStreamWriter fs = new OutputStreamWriter(new FileOutputStream("CurrentSong.txt"), "UTF-8");
            BufferedWriter out = new BufferedWriter(fs);
            out.write("    " + fullArtistAndSong + "    ");
            out.close();
        } catch (Exception e)
        {

        }
    }

}
